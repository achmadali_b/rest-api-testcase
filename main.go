package main

import (
	"fmt"
	"net/http"

	"github.com/labstack/echo/v4"
)

type Resp struct {
	Message string      `json:"message"`
	Data    interface{} `json:"data"`
}

func main() {
	e := echo.New()

	e.GET("checkHealth", checkHealth)

	fmt.Println("App start on port : ", e.Start(":3000"))
}

func checkHealth(ctx echo.Context) error {
	return ctx.JSON(http.StatusOK, Resp{"success", nil})
}
